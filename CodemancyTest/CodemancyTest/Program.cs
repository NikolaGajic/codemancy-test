﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CodemancyTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputValue = string.Empty;
            int numericValue;
            List<int> inputList = new List<int>();
            int[] inputArray;

            Console.WriteLine("Please input the array to sort.");

            while ((!inputValue.Equals("s") && !inputValue.Equals("S")) || inputList.Count < 2 )
            {
                Console.WriteLine("Input the next number or input letter 's' to proceed with sorting");
                inputValue = Console.ReadLine();

                if (inputValue.Equals("s") || inputValue.Equals("S"))
                {
                    Console.WriteLine("The array must contain at leat two elements");
                }
                else if (!int.TryParse(inputValue, out numericValue))
                {
                    Console.WriteLine("Please enter a whole number.");
                }
                else
                {
                    inputList.Add(numericValue);
                }
                
            }

            Console.WriteLine("The input array is:");
            inputList.ForEach(x => Console.WriteLine(x));

            inputArray = inputList.ToArray();

            SortAlgorithms.MergeSort(inputArray, 0, inputList.Count - 1);

            Console.WriteLine("The result array is:");
            for (int i = 0; i < inputArray.Length; i++)
            {
                Console.WriteLine(inputArray[i]);
            }

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
